/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JSpinnerEx1 {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Spinner Example");
        SpinnerModel value = new SpinnerNumberModel(5,0,10,1);
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100,100,50,30);
        frm.add(spinner);
        frm.setSize(300,300);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setLayout(null);
        frm.setVisible(true);
    }
}
