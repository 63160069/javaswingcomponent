/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JTextFieldEx2 implements ActionListener {
    JTextField tf1,tf2,tf3;
    JButton btn1,btn2;
    
    JTextFieldEx2(){
        JFrame frame = new JFrame();
        tf1 = new JTextField();
        tf1.setBounds(50,50,150,20);
        tf2 = new JTextField();
        tf2.setBounds(50,100,150,20);
        tf3 = new JTextField();
        tf3.setBounds(50,150,150,20);
        btn1 = new JButton("+");
        btn1.setBounds(50,200,50,50);
        btn2 = new JButton("-");
        btn2.setBounds(120,200,50,50);
        btn1.addActionListener(this);
        btn2.addActionListener(this);
        frame.add(tf1);
        frame.add(tf2);
        frame.add(tf3);
        frame.add(btn1);
        frame.add(btn2);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s1 = tf1.getText();
        String s2 = tf2.getText();
        int a = Integer.parseInt(s1);
        int b = Integer.parseInt(s2);
        int c = 0;
        if(e.getSource()==btn1){
            c = a+b;
        }else if (e.getSource()==btn2){
            c=a-b;
        }
        String result = String.valueOf(c);
        tf3.setText(result);
    }
    public static void main(String[] args) {
        new JTextFieldEx2();
    }
    
}
