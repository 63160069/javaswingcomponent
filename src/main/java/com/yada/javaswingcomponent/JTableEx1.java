/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JTableEx1 {
    JFrame frm;
    JTableEx1(){
        frm = new JFrame();
        String data[][] ={ {"101","Amit","670000"},    
                          {"102","Jai","780000"},    
                          {"101","Sachin","700000"}}; 
        String column[] = {"ID","NAME","SALARY"};
        JTable jt = new JTable(data,column);
        jt.setBounds(30,40,200,300);
        JScrollPane sp = new JScrollPane(jt);
        frm.add(sp);
        frm.setSize(300,400);
        frm.setVisible(true);
    }
    public static void main(String[] args) {
        new JTableEx1();
    }
}
