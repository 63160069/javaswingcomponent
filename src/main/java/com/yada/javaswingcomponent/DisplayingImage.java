/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.*;
import javax.swing.JFrame;
/**
 *
 * @author ASUS
 */
public class DisplayingImage extends Canvas {
    @Override
    public void paint(Graphics g) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("D:\\earth.gif");
        g.drawImage(i, 120, 100, this);
    }
    public static void main(String[] args) {
        DisplayingImage m = new DisplayingImage();
        JFrame frm = new JFrame();
        frm.add(m);
        frm.setSize(400,400);
        frm.setVisible(true);
    }
}
