/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.FlowLayout;  
import java.awt.event.ItemEvent;  
import java.awt.event.ItemListener;  
import javax.swing.JFrame;  
import javax.swing.JToggleButton;  
/**
 *
 * @author ASUS
 */
public class JToggleButtonEx extends JFrame implements ItemListener {
    public static void main(String[] args) {
        new JToggleButtonEx();
    }
    
    private JToggleButton btn;
    JToggleButtonEx(){
        setTitle("JToggleButton with ITemListener Example");
        setLayout(new FlowLayout());
        setJToggleButton();
        setAction();
        setSize(200,200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void setJToggleButton() {
        btn = new JToggleButton("ON");
        add(btn);
    }
    
    private void setAction() {
        btn.addItemListener(this);
    }
    @Override
    public void itemStateChanged(ItemEvent e) {
        if(btn.isSelected())
            btn.setText("OFF");
        else
            btn.setText("ON");
    }
    
}
