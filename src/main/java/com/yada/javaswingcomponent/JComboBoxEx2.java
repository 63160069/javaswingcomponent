/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JComboBoxEx2 {
    JFrame frm;
    JComboBoxEx2(){
        frm = new JFrame("ComboBox Example");
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(400,100);
        JButton btn = new JButton("Show");
        btn.setBounds(200,100,75,20);
        String languages[] = {"C","C++","C#","Java","PHP"};
        final JComboBox cb = new JComboBox(languages);
        cb.setBounds(50,100,90,20);
        frm.add(cb);
        frm.add(lbl);
        frm.add(btn);
        frm.setLayout(null);
        frm.setSize(350,350);
        frm.setVisible(true);
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               String data = "Programming language Selected: "+cb.getItemAt(cb.getSelectedIndex());
               lbl.setText(data);
            }
            
        });
    }
    public static void main(String[] args) {
        new JComboBoxEx2();
    }
}
