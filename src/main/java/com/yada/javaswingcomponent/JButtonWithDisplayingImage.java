/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JButtonWithDisplayingImage {
    JButtonWithDisplayingImage(){
        JFrame frame = new JFrame("Button Example");
        JButton btn = new JButton(new ImageIcon("D:\\icon.png"));
        btn.setBounds(100,100,100,100);
        frame.add(btn);
        frame.setSize(300,400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new JButtonWithDisplayingImage();
    }
}
