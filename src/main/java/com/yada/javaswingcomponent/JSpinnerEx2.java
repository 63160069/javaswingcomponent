/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
/**
 *
 * @author ASUS
 */
public class JSpinnerEx2 {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Spinner Example");
        
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(250,100);
        
        SpinnerModel value = new SpinnerNumberModel(5,0,10,1);
        
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100,100,50,30);
        
        frm.add(spinner);
        frm.add(lbl);
        frm.setSize(300,300);
        frm.setLayout(null);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
        
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                lbl.setText("Value :"+((JSpinner)e.getSource()).getValue());
            }
            
        });
    }
}
