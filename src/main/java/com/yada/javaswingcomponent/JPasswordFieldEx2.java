/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JPasswordFieldEx2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        final JLabel lbl = new JLabel();
        lbl.setBounds(20,150,200,50);
        final JPasswordField value = new JPasswordField();
        value.setBounds(100,75,100,30);
        JLabel lblUser = new JLabel("Username:");
        lblUser.setBounds(20,20,80,30);
        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setBounds(20,75,80,30);
        JButton btn = new JButton("Login");
        btn.setBounds(100,120,80,30);
        final JTextField txt = new JTextField();
        txt.setBounds(100,20,100,30);
        frame.add(value);
        frame.add(lblUser);
        frame.add(lbl);
        frame.add(lblPassword);
        frame.add(btn);
        frame.add(txt);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username: " + txt.getText();
                data += ", Password: " + new String(value.getPassword());
                lbl.setText(data);
            }
            
        });
        
    }
}
