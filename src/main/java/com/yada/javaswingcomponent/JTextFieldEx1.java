/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JTextFieldEx1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("TextField Example");
        JTextField txt1,txt2;
        txt1 = new JTextField("Welcome to ");
        txt1.setBounds(50,100,200,30);
        txt2 = new JTextField("Java");
        txt2.setBounds(50,150,200,30);
        frame.add(txt1);
        frame.add(txt2);
        frame.setSize(400,400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
