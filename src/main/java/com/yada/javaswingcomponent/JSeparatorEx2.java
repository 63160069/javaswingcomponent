/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.*;
/**
 *
 * @author ASUS
 */
public class JSeparatorEx2 {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Separator Example");
        frm.setLayout(new GridLayout(0,1));
        
        JLabel lbl1 = new JLabel("Above Separator");
        frm.add(lbl1);
        
        JSeparator sep = new JSeparator();
        frm.add(sep);
        
        JLabel lbl2 = new JLabel("Below Separator");
        frm.add(lbl2);
        
        frm.setSize(400,100);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
}
