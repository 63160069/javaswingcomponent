/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JLabelEx2  extends Frame implements ActionListener {
    JTextField tf;
    JLabel lbl;
    JButton btn;
    JLabelEx2() {
        tf = new JTextField();
        tf.setBounds(50,50,150,20);
        lbl = new JLabel();
        lbl.setBounds(50,100,250,20);
        btn = new JButton("Find IP");
        btn.setBounds(50,150,95,30);
        btn.addActionListener(this);
        add(btn);
        add(tf);
        add(lbl);
        setSize(400,400);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            String host = tf.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();
            lbl.setText("IP of "+host+" is: "+ip);
        }catch(Exception ex) {
            System.out.println(ex);
        }
    }
    public static void main(String[] args) {
        new JLabelEx2();
    }
}
