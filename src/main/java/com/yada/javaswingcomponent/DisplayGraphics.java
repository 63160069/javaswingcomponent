/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.*;
import javax.swing.JFrame;
/**
 *
 * @author ASUS
 */
public class DisplayGraphics extends Canvas {
    @Override
    public void paint(Graphics g ) {
        g.drawString("Hello", 40, 40);
        setBackground(Color.WHITE);
        g.fillRect(130, 30, 100, 80);
        g.drawOval(30, 130, 50, 60);
        setForeground(Color.BLUE);
        g.fillOval(130, 130, 50, 60);
        g.drawArc(30, 200, 40, 50, 90, 60);
        g.fillArc(30, 130, 40, 50, 180, 40);
    }
    public static void main(String[] args) {
        DisplayGraphics m = new DisplayGraphics();
        JFrame frm = new JFrame();
        frm.add(m);
        frm.setSize(400,400);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
    }
}
