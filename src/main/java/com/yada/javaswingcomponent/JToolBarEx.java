/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.BorderLayout;  
import java.awt.Container;  
import javax.swing.JButton;  
import javax.swing.JComboBox;  
import javax.swing.JFrame;  
import javax.swing.JScrollPane;  
import javax.swing.JTextArea;  
import javax.swing.JToolBar;
/**
 *
 * @author ASUS
 */
public class JToolBarEx {
    public static void main(final String[] args) {
        JFrame myfrm = new JFrame("JToolBar Example");
        myfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);
        
        JButton btn = new JButton("File");
        toolbar.add(btn);
        toolbar.addSeparator();
        toolbar.add(new JButton("Edit"));
        toolbar.add(new JComboBox(new String[] {"Opt-1","Opt-2","Opt-3","Opt-4"}));
        
        Container contentPane = myfrm.getContentPane();
        contentPane.add(toolbar,BorderLayout.NORTH);
        
        JTextArea txtArea = new JTextArea();
        JScrollPane myPane = new JScrollPane(txtArea);
        contentPane.add(myPane,BorderLayout.EAST);
        myfrm.setSize(450,250);
        myfrm.setVisible(true);
    }
}
