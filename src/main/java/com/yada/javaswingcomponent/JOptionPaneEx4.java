/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ASUS
 */
public class JOptionPaneEx4 extends WindowAdapter {
    JFrame frm;
    JOptionPaneEx4() {
        frm = new JFrame();
        frm.addWindowListener(this);
        frm.setSize(300,300);
        frm.setLayout(null);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }
    @Override
    public void windowClosing(WindowEvent e) {
        int a = JOptionPane.showConfirmDialog(frm, "Are you sure?");
        if (a == JOptionPane.YES_OPTION) {
            frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
    public static void main(String[] args) {
        new JOptionPaneEx4();
    }
}
