/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import javax.swing.*;
/**
 *
 * @author ASUS
 */
public class JComboBoxEx1 {
    JFrame frm;
    JComboBoxEx1() {
        frm = new JFrame("ComboBox Example");
        String country[] = {"India","Aus","U.S.A","England","Newzealand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50,50,90,20);
        frm.add(cb);
        frm.setLayout(null);
        frm.setSize(400,500);
        frm.setVisible(true);
    }
    public static void main(String[] args) {
        new JComboBoxEx1();
    }
}
