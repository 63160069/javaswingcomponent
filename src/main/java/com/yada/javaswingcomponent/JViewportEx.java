/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.javaswingcomponent;
import java.awt.BorderLayout;  
import java.awt.Color;  
import java.awt.Dimension;  
import javax.swing.JButton;  
import javax.swing.JFrame;  
import javax.swing.JLabel;  
import javax.swing.JScrollPane;  
import javax.swing.border.LineBorder;  
/**
 *
 * @author ASUS
 */
public class JViewportEx {
    public static void main(String[] args) {
        JFrame frm = new JFrame("Tabbed Pane Sample");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lbl = new JLabel("Label");
        lbl.setPreferredSize(new Dimension(1000,1000));
        JScrollPane jScrollPane = new JScrollPane(lbl);
        
        JButton jButton = new JButton();
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane.setViewportBorder(new LineBorder(Color.RED));
        jScrollPane.getViewport().add(jButton,null);
        
        frm.add(jScrollPane,BorderLayout.CENTER);
        frm.setSize(400,150);
        frm.setVisible(true);
    }
}
